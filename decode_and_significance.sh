#!/bin/bash
# Set up LM, decode, test statistical significance
# for all 4 LMs

dir0=/g/tial/sw/pkgs/kaldi/src/feat/
dir1=/g/tial/sw/pkgs/kaldi/src/featbin/
dir2=/g/tial/projects/atc/ldc_atc/kaldi_cuinn/egs/ldc_atc/
dir3=/g/tial/projects/atc/ldc_atc/kaldi_cuinn/egs/ldc_atc/exp/chain_online_cmn_specaug/tdnn1k_sp/

cd $dir0
make
cd $dir1
make
cd $dir2

data=GND_test
GND_datadir=GND_test
decode_cmd=run.pl
exp=nnet3_online_cmn_specaug
frames_per_chunk=140
tree_dir=exp/chain_online_cmn_specaug/tree_sp
nnet3_affix=_online_cmn_specaug
train_cmd=run.pl
lm_order=3 
local=data/local 
lang=data/lang_ldc_GND

WER="100.0"
for thresh in -0.05
  do
    echo "**************** OMITTING BIN = $thresh *******************"
	for LM in 'ldc_full_10GND'
		  #'ldc_full_1GND' \
		  #'ldc_full_10GND' \
		  #'ldc_full_10GND_parsed_paraphrased_exp1a';
	  do
            # change threshold
            sed -i "s/^--silence-threshold.*/--silence-threshold=${thresh}/g" conf/mfcc_hires_histmatch.conf
            cat conf/mfcc_hires_histmatch.conf		    


	    lm=$LM 
	    corpus=corpus_${lm}
	    vocab=$lang/vocab-full.txt

	    KALDI_ROOT=/g/tial/sw/pkgs/kaldi


	    export PATH=$PWD/utils/:$KALDI_ROOT/src/bin:$KALDI_ROOT/tools/openfst/bin:$KALDI_ROOT/src/fstbin/:$KALDI_ROOT/src/gmmbin/:$KALDI_ROOT/src/featbin/:$KALDI_ROOT/src/lmbin/:$KALDI_ROOT/src/sgmm2bin/:$KALDI_ROOT/src/fgmmbin/:$KALDI_ROOT/src/latbin/:$PWD:$PATH
	  
	    export LD_LIBRARY_PATH="/g/tial/sw/pkgs/liblbfgs-1.10/lib:$LD_LIBRARY_PATH" 
	    export SRILM="/g/tial/sw/pkgs/srilm-1.7.1" 
	    export MACHINE_TYPE="i686-m64" 
	    export PATH="$SRILM/bin:$SRILM/bin/$MACHINE_TYPE:$PATH" 
	    export MANPATH="$MANPATH:$SRILM/man"


	    ngram-count -order $lm_order -vocab $vocab -kndiscount -text $local/${corpus}.txt -lm $local/tmp/lm.arpa

	    arpa2fst --disambig-symbol=#0 --read-symbol-table=$lang/words.txt $local/tmp/lm.arpa $lang/G.fst

	    utils/mkgraph.sh  --self-loop-scale 1.0  $lang  $tree_dir $tree_dir/graph_tgsmall


	    echo "*********************** LM = $LM HAS BEEN SETUP ************************"

	    for folder in 'yes_histmatch_GND_test' # 'no_histmatch_GND_test';
	      do
		
		echo "**************** folder = $folder ***********************"

		utils/copy_data_dir.sh data/${GND_datadir} data/${GND_datadir}_hires
		yes | rm /data/ldc_snapshot/*.txt
		if [[ $folder == "yes_histmatch_GND_test" ]]; then
		  steps/make_mfcc.sh --nj 1 --mfcc-config conf/mfcc_hires_histmatch.conf \
		    --cmd "$train_cmd" data/${GND_datadir}_hires
	 
		  echo '*********************** MFCCs HISTMATCH MADE **************************'
		else
		  steps/make_mfcc.sh --nj 1 --mfcc-config conf/mfcc_hires.conf \
		    --cmd "$train_cmd" data/${GND_datadir}_hires 
		
		  echo '*************************** MFCCs MADE ******************************'
		fi
		steps/compute_cmvn_stats.sh data/${GND_datadir}_hires

		utils/fix_data_dir.sh data/${GND_datadir}_hires

		steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 1 \
		  data/${GND_datadir}_hires exp/nnet3${nnet3_affix}/extractor \
		  exp/nnet3${nnet3_affix}/ivectors_${GND_datadir}_hires

		steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 --frames-per-chunk 140 --nj 1 --cmd run.pl --num-threads 4 --online-ivector-dir exp/nnet3_online_cmn_specaug/ivectors_GND_test_hires exp/chain_online_cmn_specaug/tree_sp/graph_tgsmall data/GND_test_hires exp/chain_online_cmn_specaug/tdnn1k_sp/decode_${folder} | tee decode_output.out



		echo "***************** DECODE LM = ${LM}, HIST = ${folder} ***********************"
	     

	        exp=chain_online_cmn_specaug/tdnn1k_sp

	        cd ${dir3}decode_${folder}
		
	        echo "*************** MOVING TO DIRECTORY ${dir3}decode_${folder} ******************"
	       
	        cp -r lat.1.gz log num_jobs scoring wer_* $lm
	        cd $dir2

	        echo "******************** MOVING TO DIRECTORY $dir2 *****************************"

	        x=$folder
	       
	        cat exp/$exp/decode_$x/$lm/scoring/10.tra | utils/int2sym.pl -f 2- $lang/words.txt | sed 's:\<UNK\>::g' > exp/$exp/decode_$x/$lm/scoring/10.txt
	      done
	     
	    x1=no_histmatch_GND_test
	    x2=yes_histmatch_GND_test
	    exp=chain_online_cmn_specaug/tdnn1k_sp

	    echo "**************** SIGNIFICANCE RESULTS LM = $LM  *********************"

	    src/bin/compute-wer-bootci --mode=present ark:exp/$exp/decode_${x1}/$lm/scoring/test_filt.txt ark:exp/$exp/decode_${x1}/$lm/scoring/10.txt ark:exp/$exp/decode_${x2}/$lm/scoring/10.txt | tee decode_output_significance.out
	  
	      #WER="100.0"
	      input="decode_output.out"
	      while IFS= read -r line
		do
		  if [[ "$line" == *"WER"* ]]; then
		    WER=$(echo $line | sed 's/ 95%.*//g' | sed 's/.*%WER //g')            
	            echo "thresh = $thresh WER = $WER" >> thresh_WERs_${LM}_${data}.txt		   
                    #sed -i "s/^--omit-bin-first.*/--omit-bin-first=${omit}/g" conf/mfcc_hires_histmatch.conf
		    #sed -i "s/^--omit-bin-second.*/--omit-bin-second=${omit}/g"   conf/mfcc_hires_histmatch.conf
		    #sed -i "s/^--omit-bin-third.*/--omit-bin-third=${omit}/g"   conf/mfcc_hires_histmatch.conf
                    #sed -i "s/^--silence-threshold.*/--silence-threshold=${omit}/g" conf/mfcc_hires_histmatch.conf
		    
                    #cat conf/mfcc_hires_histmatch.conf		    

 
		    break
		  fi
	       #   echo "***************************ECHOOOOOING OUTPUT***********************************"
		done < "$input"
	      input="decode_output_significance.out"
              while IFS= read -r line
                do
                  if [[ "$line" == *"Probability of"* ]]; then
                    PROB=$(echo $line | sed 's/.*improving Set1: //g')
                    echo "thresh = $thresh PROB = $PROB" >> thresh_WERs_${LM}_${data}.txt
                    break
                  fi
                done < "$input"

            done

  done


